function validation_inscription_club_velomane()
{
	if(document.form1.nom.value=='')
	{
		alert('Vous devez inscrire nom !');
		document.form1.nom.focus();
	}
	else if(document.form1.courriel.value=='')
	{
		alert('Vous devez inscrire votre courriel !');
		document.form1.courriel.focus();
	}
	else
	{
		document.form1.method = "POST";
		document.form1.action = "pageClubVelomane_update.php";
		document.form1.submit();
	}
}
function validation_inscription_liquidation_vip()
{
	if(document.form1.nom.value=='')
	{
		alert('Vous devez inscrire nom !');
		document.form1.nom.focus();
	}
	else if(document.form1.courriel.value=='')
	{
		alert('Vous devez inscrire votre courriel !');
		document.form1.courriel.focus();
	}
	else
	{
		document.form1.method = "POST";
		document.form1.action = "pageLiquidationVip_update.php";
		document.form1.submit();
	}
}
function validation_contact()
{
	if(document.form1.nom.value=='')
	{
		alert('Vous devez inscrire nom !');
		document.form1.nom.focus();
	}
	else if(document.form1.courriel.value=='')
	{
		alert('Vous devez inscrire votre courriel !');
		document.form1.courriel.focus();
	}
	else
	{
		document.form1.method = "POST";
		document.form1.action = "pageContact_update.php";
		document.form1.submit();
	}
}

function messageAlerte(type, wrongData) {
	var message = "Erreur, type invalide"
	switch (type) {
		case "nom":
			message = "Vous devez inscrire votre nom!";
			break;
		case "courriel":
			message = "Vous devez inscrire votre courriel!";
			break;
		case "telephone":
			message = "Vous devez inscrire num�ro de t�l�phone!";
			break;
		case "adresse": 
			message = "Vous devez inscrire votre adresse de r�sidence!";
			break;
		case "edition":
			message = "Vous devez choisir l'�dition du v�lo!";
			break;
		case "cadre_couleur": 
			message = "Vous devez choisir la couleur du cadre!";
			break;
		case "roues_couleur": 
			message = "Vous devez choisir la couleur des roues!";
			break;
		case "groupe": 
			message = "Vous devez choisir le groupe Shimano!";
			break;
		case "bras": 
			message = "Vous devez fournir la longueur du bras!";
			break;
		case "jambe": 
			message = "Vous devez fournir la longueur de la jambe!";
			break;
		case "dos": 
			message = "Vous devez fournir la longueur du dos!";
			break;
		case "epaules": 
			message = "Vous devez fournir la largeur des �paules!";
			break;
		case "grandeur":
			message = "Vous devez fournir votre grandeur!";
			break;
		case "grandeur_pouces":
			message = "Vous devez fournir votre grandeur (pouces)!";
			break;
		case "poids":
			message = "Vous devez fournir votre poids!";
			break;
	}
	if (wrongData) message += " (Chiffres seulement)";
	alert(message);
}

function validation_commande()
{
	var post = true;
	var fieldName;
	if (document.form1.grandeur_unite.value == "pieds") {
		var formFields = ["nom", "courriel", "telephone", "adresse", "edition", "cadre_couleur", "roues_couleur", "groupe", "bras", "jambe", "dos", "epaules", "grandeur", "grandeur_pouces", "poids"];
		var formFieldsNum = ["bras", "jambe", "dos", "epaules", "grandeur", "grandeur_pouces", "poids"];
	}
	else {
		var formFields = ["nom", "courriel", "telephone", "adresse", "edition", "cadre_couleur", "roues_couleur", "groupe", "bras", "jambe", "dos", "epaules", "grandeur", "poids"];
		var formFieldsNum = ["bras", "jambe", "dos", "epaules", "grandeur", "poids"];
	}
	
	//Check if isNaN
	for (i = 0; i < formFieldsNum.length; i++) { 
		fieldName = formFieldsNum[i];
		if (isNaN(document.form1[fieldName].value)) {
			messageAlerte(fieldName, true);
			document.form1[fieldName].focus();
			i = formFieldsNum.lenght;
			post = false;
		} 
	}
	
	if (post) {
		//Check if empty
		for (i = 0; i < formFields.length; i++) { 
			fieldName = formFields[i];
			//Removes spaces
			var fieldChecked = document.form1[fieldName].value.replace(/ /g,'')
			if (fieldChecked=='') {
				messageAlerte(fieldName, false);
				document.form1[fieldName].focus();
				i = formFields.lenght;
				post = false;
			} 
		}
	}
	
	if (post) {
		document.form1.method = "POST";
		document.form1.action = "commande_update.php";
		document.form1.submit();
	}
}
