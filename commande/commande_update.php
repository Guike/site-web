<?PHP
	//Informations
	$nom = strip_tags($_POST['nom']);
	$courriel = strip_tags($_POST['courriel']);
	$telephone = strip_tags($_POST['telephone']);
	$adresse = strip_tags($_POST['adresse']);
	
	$edition = $_POST['edition'];
	$cadre_couleur = $_POST['cadre_couleur'];
	$roues_couleur = $_POST['roues_couleur'];
	$groupe = $_POST['groupe'];
	
	$bras = strip_tags($_POST['bras']);
	$bras_unite = $_POST['bras_unite'];
	$jambe = strip_tags($_POST['jambe']);
	$jambe_unite = $_POST['jambe_unite'];
	$dos = strip_tags($_POST['dos']);
	$dos_unite = $_POST['dos_unite'];
	$epaules = strip_tags($_POST['epaules']);
	$epaules_unite = $_POST['epaules_unite'];
	
	$grandeur = strip_tags($_POST['grandeur']);
	$grandeur_unite = $_POST['grandeur_unite'];
	if ($grandeur_unite == 'pieds') {
		$grandeur_pouces = strip_tags($_POST['grandeur_pouces']);
	}
	$poids = strip_tags($_POST['poids']);
	$poids_unite = $_POST['poids_unite'];
	
	//Fonctions
	function convertToPo($mesure) {	
		$mesure_unite = $mesure . "_unite";
		global $$mesure_unite;
		$unite = $$mesure_unite;
		
		if ($unite == "cm") {
			global $$mesure;
			$$mesure = $$mesure/2.54;
			$$mesure = number_format($$mesure, 3);
		}
	}
	
	//Envoi du e-mail
	if($nom != "" || $courriel != "" || $telephone != "" || $adresse != "" || $edition != "" || $cadre_couleur != "" || $roues_couleur != "" || $groupe != "" || $bras != "" || $jambe != "" || $dos != "" || $epaules != "" || $grandeur != "" || $poids != "" || (isset($grandeur_pouces) && $grandeur_pouces != ""))
	{
		//Conversions cm/pieds
		if (!isset($grandeur_pouces)) {
			//Pieds en d�cimal
			$pieds = ($grandeur/30.48);			
			$grandeur = floor($pieds);
			
			//Conversion fraction de pieds/pouces
			$pouces = ($pieds - $grandeur);
			$grandeur_pouces = (12*$pouces);
			
			//Conserver 3 d�cimales
			$grandeur_pouces = number_format( $grandeur_pouces, 3  );
		}
		
		//Conversion cm/po
		$mesures = array("bras", "jambe", "dos", "epaules");
		for ($i=0; $i<count($mesures); $i++) {
			convertToPo($mesures[$i]);
		}
		
		//Conversion kg/lbs
		if ($poids_unite == kg) {
			$poids = $poids*2.2;
		}
		
		//Message pour le magasin
		$email="velomane@velomane.com";

		$MailTo = $email; //adresse � laquelle sera envoy� le contenu du formulaire
		$MailSubject = "Nouvelle commande (Site-web)"; //texte qui va figurer dans le champ "sujet" du email
		$MailHeader = "From: contact@velomane.com\r\n"; //adresse email qui va figurer dans le champ "exp�diteur" du email et qui peut �tre remplac� par la variable "$champx" ("$champ3").
		$MailHeader .= "MIME-Version: 1.0\r\n";
		$MailHeader .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$MailBody = "<html><body><div style='font-size:16px'>";
		$MailBody.= "Bonjour M. Caron, vous avez re�u une nouvelle commande de v�lo 2016<br><br>";
		
		$MailBody.="<b>Le client:</b><br>";
		$MailBody.="Nom: $nom<br>";
		$MailBody.="Adresse courriel: $courriel<br>";
		$MailBody.="Num�ro de t�l�phone: $telephone<br>";
		$MailBody.="Adresse de r�sidence: $adresse<br><br>";
		
		$MailBody.="<b>Le mod�le:</b><br>";
		$MailBody.="�dition: $edition<br>";
		$MailBody.="Couleur du cadre: $cadre_couleur<br>";
		$MailBody.="Couleur des roues: $roues_couleur<br>";
		$MailBody.="Groupe: $groupe<br><br>";
		
		$MailBody.="<b>Les mesures:</b><br>";
		$MailBody.="Bras: $bras pouces<br>";
		$MailBody.="Jambe: $jambe pouces<br>";	
		$MailBody.="Dos: $dos pouces<br>";		
		$MailBody.="�paules: $epaules pouces<br>";		
		$MailBody.="Grandeur: $grandeur pieds $grandeur_pouces pouces<br>";
		$MailBody.="Poids: $poids livres<br><br>";
		
		$MailBody.="Bonne journ�e !";
		$MailBody.= "</div></body></html>";
		mail($MailTo, $MailSubject, $MailBody, $MailHeader); //envoi du message
	}
	else echo "Erreur de validation";
?>

<script language="Javascript">
<!--
document.location.replace("commande.php?c1=1");
// -->
</script>