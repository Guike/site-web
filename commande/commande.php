<html>
<head>
<meta name="robots" content="index, follow">
<meta name="identifier-url" content="http://www.velomane.com">
<meta name="Author" content="http://www.serviceswebquebec.com">
<meta name="keywords" content="V�lomane, Le v�lomane, v�lo, cadre de carbone, manufacturier, v�lo sur mesure, mises au point, frein à disque , Montr�al, r�parations de v�lo, v�lo de comp�tition, v�lo de triathlon,v�lo de montagne, cyclotourisme">
<meta name="copyright" content="V�lomane">
<meta name="description" content="V�lomane- manufacturier et d�taillant de v�lo sur mesure depuis 1982">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--[if gte IE 6]>
<script src="dynActiveX.js"></script>
<![endif]-->
<title>V�lomane - Commande</title>
<link rel="stylesheet" href="styles.css">
<script type="text/javascript" src="controle_form.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script>
	function changeUnits() {
		jQuery('.alternateUnit').toggle();
	}
	function changeOptions() {
		var active = document.getElementById('edition').value;
		//Reset the field
		jQuery('#cadre_couleur').prop('selectedIndex',0);
		jQuery('#cadre_couleur').addClass("noSelect");	
		
		//Enable the right options
		jQuery('#cadre_couleur option').addClass("hidden");
		jQuery('.' + active).removeClass("hidden");
	}
	function changeColor(formField) {
		jQuery('#' + formField).removeClass("noSelect");
	}
	function changePrice() {
		//Tableau des prix
		var modele = [];
		modele["U2"] = ["Veuillez choisir un groupe.", "1695$", "1895$", "1995$"];
		modele["Absolut"] = ["Veuillez choisir un groupe.", "1295$", "1395$", "1495$"];
		
		var selectedIndex = form1.elements["groupe"].selectedIndex;
		var edition = document.getElementById('edition').value;
		var prix = "Prix: ";
		
		if(edition == "") prix += "Veuillez choisir un mod�le.";	
		else prix += modele[edition][selectedIndex];
		
		document.getElementById('prix').innerHTML = prix;
	}
</script>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
<?PHP
if(!isset($_GET['c1']))
{
	$c1=0;
}
	else
	{
		$c1=$_GET['c1'];
	}
if(!isset($_GET['c2']))
{
	$c2=0;
}
	else
	{
		$c2=$_GET['c2'];
	}
if(!isset($_GET['c3']))
{
	$c3=0;
}
	else
	{
		$c3=$_GET['c3'];
	}
?>
<table background="images/fond.jpg" align="center" width="999" cellpadding="0" cellspacing="0" border="0" height="725">
	<tr>
		<td valign="top" width="215">
			<?PHP include('s_menu1.php'); ?>
		</td>
		<td align="right" valign="top" width="215">
			<?PHP include('s_menu2.php'); ?>
		</td>
		<td valign="top">
			<?PHP include('s_haut.php'); ?><br>
			<table width="100%" border="0"><tr><td>
				<table width="100%" border="0">
					<tr>
						<td width="52"></td>
						<td background="images/fond1.png" height="500" valign="top">
							<table align="center" border="0">
								<tr>
									<td width="60%">  <!-- Form status feedback-->
									<?PHP
									if(isset($_GET['c1']))
									{
										?>
										<font color="#AA0000" size="4">
											<b>Votre commande a �t� envoy�e! Nous vous contacterons sous peu.</b><br><br>
										</font>
										<?PHP
									}
									?>
									</td>
								</tr>
								<tr>
									<td width="60%">
										<font size="5"><b>Commander un v�lo �dition 2016</b></font><br><br>
										<!-- Formulaire http://www.w3.org/TR/WCAG20-TECHS/SCR19.html -->
										<form method="POST" name="form1" enctype="multipart/form-data">
											<b class="font_2">Votre nom:</br></b>
											<input name="nom" class="formField inputText"></br>
											<b class="font_2">Adresse courriel:</b></br>
											<input name="courriel" class="formField inputText"></br>
											<b class="font_2">Num�ro de t�l�phone:</b></br>
											<input name="telephone" class="formField inputText"></br>
											<b class="font_2">Adresse de r�sidence:</b></br>
											<input name="adresse" class="formField inputText"></br><br>
											
											<b class="font_2">�dition du v�lo:</br></b>
											<select id="edition" class="formField noSelect selectModel" name="edition" onchange="changeColor(this.id); changePrice(); changeOptions();">
												<option value="" selected class="hidden">Choisir une �dition</option>
												<option value="U2">U2</option>
												<option value="Absolut">Absolut</option>											
											</select><br>
											<b class="font_2">Couleur du cadre:</b></br>
											<select id="cadre_couleur" class="formField noSelect selectModel" name="cadre_couleur" onchange="changeColor(this.id);">
												<option value="" selected class="hidden">Choisir une couleur</option>
												<option class='hidden U2' value="Noir mat/rouge">Noir mat/rouge</option>
												<option class='hidden U2' value="Blanc/rouge">Blanc/rouge</option>
												<option class='hidden U2' value="Rouge/blanc">Rouge/blanc</option>												
												<option class='hidden U2' value="Noir mat">Noir mat</option>
												<option class='hidden Absolut' value="Blanc/rouge">Blanc/rouge</option>
												<option class='hidden Absolut' value="Gris/rouge">Gris/rouge</option>
												<option class='hidden Absolut' value="Noir/rouge">Noir/rouge</option>												
												<option class='hidden Absolut' value="Noir mat">Noir mat</option>
											</select><br>
											<b class="font_2">Couleur des roues:</b></br>
											<select id="roues_couleur" class="formField noSelect selectModel" name="roues_couleur" onchange="changeColor(this.id);">
												<option value="" selected class="hidden">Choisir une couleur</option>
												<option value="Noir">Noir</option>
												<option value="Rouge">Rouge</option>
												<option value="Orange">Orange</option>
											</select><br>
											<b class="font_2">Groupe Shimano:</b></br>
											<select id="groupe" class="formField noSelect selectModel" name="groupe" onchange="changeColor(this.id); changePrice();">
												<option value="" selected class="hidden">Choisir un groupe</option>
												<option value="Tiagra">Shimano Tiagra</option>
												<option value="105">Shimano 105</option>
												<option value="Ultegra">Shimano Ultegra</option>
											</select></br><br>
											
											<div class="formField" id="prix">Prix: Veuillez choisir un mod�le et un groupe.</div><br>
											
											<b>Comment proc�der pour les mesures: </b>
											<p style="text-align:justify">Pour s'assurer de livrer un produit parfait pour vous, nous avons besoin de quelques mesures. Tout d'abord, il faut conna�tre la largeur des �paules, la longueur du bras, la longueur de la jambe et la hauteur du dos. Le sch�ma ci-dessous indique comment prendre les mesures. Ensuite, pour compl�ter nos donn�es, il nous faut conna�tre votre grandeur et votre poids. Avec ces renseignement, nous sommes s�rs de pouvoir cr�er un v�lo parfait pour vous!</p>
											<img src="images/commande/tableauMesures.jpg" width="350px"><br><br>
											
											<b class="font_2">Largeur des �paules (A):</b></br>
											<input name="epaules" class="formField inputNum">
											<select class="formField selectUnit" name="epaules_unite">
												<option value="pouces">pouces</option>
												<option value="cm">cm</option>
											</select></br>
											
											<b class="font_2">Longueur du bras (B):</b></br>
											<input name="bras" class="formField inputNum">
											<select class="formField selectUnit" name="bras_unite">
												<option value="pouces">pouces</option>
												<option value="cm">cm</option>
											</select></br>
											
											<b class="font_2">Longueur de la jambe (C):</b></br>
											<input name="jambe" class="formField inputNum">
											<select class="formField selectUnit" name="jambe_unite">
												<option value="pouces">pouces</option>
												<option value="cm">cm</option>
											</select></br>
											
											<b class="font_2">Hauteur du dos (D):</b></br>
											<input name="dos" class="formField inputNum">
											<select class="formField selectUnit" name="dos_unite">												
												<option value="pouces">pouces</option>
												<option value="cm">cm</option>
											</select></br>
											
											
											
											<b class="font_2">Grandeur:</b></br>
											<input name="grandeur" class="formField inputNum">
											<select id="grandeur_unite" class="formField selectUnit" name="grandeur_unite" onchange="changeUnits();">
												<option value="pieds">pieds</option>
												<option value="cm">cm</option>
											</select>
											<input name="grandeur_pouces" class="formField inputNum alternateUnit">
											<input name="grandeur_unite_pouce" class="formField alternateUnit selectUnit" value="pouces" disabled></br>
											
											<b class="font_2">Poids:</b></br>
											<input class="formField inputNum" name="poids">
											<select class="formField selectUnit" name="poids_unite">
												<option value="lbs">livres</option>
												<option value="kg">kg</option>
											</select></br></br>
											
											<input class="formField sendButton" type="button" value=" Envoyer les informations " onClick="validation_commande()">
										</form>
									</td>
								</tr>
							</table>
						</td>
						<td width="1"></td>
					</tr>
				</table></td></tr>
			</table>
		</td>
	</tr>
	<?PHP
	include('s_bas.php');
	?>
</table>
</body>
</html>