<!DOCTYPE html>
<html lang="en">
	<body>
			<?PHP
			function mysqlSearch($conn){
				$search = $_POST['search'];
				$search = mysqli_real_escape_string($conn, $search);
				$sql = 	"SELECT * FROM tag_lst WHERE tag_id = '$search' OR tag_nom LIKE '%$search%' ORDER BY tag_date DESC";
				
				$retval = mysqli_query($conn, $sql);
				if(! $retval )
				{
					die('Could not read data: ' . mysqli_error($conn));
				}
				
				$nResults = mysqli_num_rows($retval);
				echo "<div class='status'­>$nResults résultat";

				if ($nResults == 0) {
					echo " pour '$search'. Essayez une autre recherche. </div>";
				}
				else if ($nResults == 1) {
					echo ": </div><div class='clear'></div>";
				}
				else {
					echo "s: </div><div class='clear'></div>";
				}
				
				$formNo = 2;
				while($row = mysqli_fetch_array($retval, MYSQL_ASSOC))
				{
					$formNo++;
					echo <<<HEREDOC
							<div class='resultWrap'>
							<form  method='POST' name='resultForm' class='display'>
							<div class='result' id='result$formNo' href='#box$formNo'>
							Tag: <input type='text' name='id' value="{$row['tag_id']}" disabled  class='undisablable'></input><br>
							Nom: <input type='text' name='nom' value="{$row['tag_nom']}" disabled></input><br>
							Téléphone: <input type='text' name='tel' value="{$row['tag_tel']}" disabled></input><br>
							Date reçu: <span>{$row['tag_date']}</span><br>
							Description: <input type='text' name='desc' value="{$row['tag_desc']}" disabled></input><br>
							Statut: <input type='text' name='status' value="{$row['tag_status']}" disabled></input>
							</div>
							<div class='buttonWrap' id='box$formNo'>
							<input type='button' value=' Supprimer ' onclick="databaseQuery('delete', '$formNo')" class='bouton delete primary'>
							<input type='button' value=' Modifier ' onclick="openModification('$formNo')" class='bouton modify primary'>
							<input type='button' value=' Confirmer ' onclick="closeModification('$formNo', true)" class='bouton add modification'>
							<input type='button' value=' Annuler ' onclick="closeModification('$formNo', false)" class='bouton delete modification'>
							</div>
							</form>
							<div class='clear'></div>
							</div>
HEREDOC;
				}
			}

			function mysqlAdd($conn){
				if (!isset($_POST['id']) || !isset($_POST['nom']) || !isset($_POST['tel']) || !isset($_POST['desc']) || empty($_POST['id']) || empty($_POST['nom']) || empty($_POST['tel']) || empty($_POST['desc']) || !is_numeric($_POST['id'])) {
					echo "<div class='status'­>Veuillez inscrire toute l'information demandée.</div>";
				}
				else {
					$id = $_POST['id'];
					$id = mysqli_real_escape_string($conn, $id);
					
					$nom = $_POST['nom'];
					$nom = mysqli_real_escape_string($conn, $nom);
					
					$tel = $_POST['tel'];
					$tel = mysqli_real_escape_string($conn, $tel);
					
					$desc = $_POST['desc'];
					$desc = mysqli_real_escape_string($conn, $desc);

					$date = GETDATE();
					
					$status ='En attente';

					if (isset($_POST['status'])) {
						$status = $_POST['status'];
						$status = mysqli_real_escape_string($conn, $stat);
					}

					$sql = 	"REPLACE INTO tag_lst ".
							"(tag_id, tag_nom, tag_tel, tag_desc, tag_date, tag_status) ".
							"VALUES ".
							"('$id', '$nom', '$tel', '$desc', NOW(), '$status')";
									
					$retval = mysqli_query($conn, $sql);
					if(! $retval )
					{
						die('Could not add data: ' . mysqli_error($conn));
					}
					
					echo 	"<div class='status'>Tag ajouté.</div>".
							"<div class='status'>".
							"ID: $id <br> ".
							"Nom: $nom <br> ".
							"Téléphone: $tel <br> ".
							"Description: $desc <br> ".
							"</div>";
				}
			}

			function mysqlDelete($conn){
				if (!isset($_POST['id']) || empty($_POST['id'])) {
					echo "<div class='status'­>Veuillez inscrire le numéro de tag à retirer.</div>";
				}
				else {
					$id = $_POST["id"];
					$id = mysqli_real_escape_string($conn, $id);
				
					$sql = 	"DELETE FROM tag_lst WHERE tag_id=$id ";
				
					mysqli_query($conn, $sql);
					if(mysqli_affected_rows($conn) == 0)
					{
						die('Could not remove data: tag id invalid.');
					}
				
					echo 	"<div class='status'>Tag retiré.</div>".
							"<div class='status'>Id: $id</div>";
				}
			}

			function mysqlCreateTable(){
				$sql = 	"CREATE TABLE tag_lst (	tag_id int not null,
								tag_nom varchar(35) not null,
								tag_tel varchar(22) not null,
								tag_date DATE not null,
								tag_desc varchar(35) not null,								
								tag_status varchar(30) default 'En attente',
								primary key (tag_id),
									index client_nom (tag_nom)
						);";
				
				$retval = mysqli_query($conn, $sql);
				if(! $retval )
				{
					die('Could not create table: ' . mysqli_error($conn));
				}
		
				echo "<div class='status'­>Table created successfully: tag_lst</div>";
			}

			function mysqlModify($conn){
				if (!isset($_POST['id']) || empty($_POST['id']) || !isset($_POST['nom']) || !isset($_POST['tel']) || !isset($_POST['desc']) || !isset($_POST['status']) || empty($_POST['nom']) || empty($_POST['tel']) || empty($_POST['desc']) || empty($_POST['status'])) {
					echo "<div class='status'­>Veuillez inscrire toute l'information demandée.</div>";
				}
				else {
					$id = $_POST['id'];
					$id = mysqli_real_escape_string($conn, $id);

					$nom = $_POST["nom"];
					$nom = mysqli_real_escape_string($conn, $nom);
					
					$tel = $_POST["tel"];
					$tel = mysqli_real_escape_string($conn, $tel);
					
					$desc = $_POST["desc"];
					$desc = mysqli_real_escape_string($conn, $desc);

					$status = $_POST["status"];
					$status = mysqli_real_escape_string($conn, $status);
					
					$sql = 	"UPDATE tag_lst ".
							"SET tag_nom='$nom', tag_tel='$tel', tag_desc='$desc', tag_status='$status' ".
							"WHERE tag_id='$id'";
									
					$retval = mysqli_query($conn, $sql);
					if(! $retval )
					{
						die('Could not modify data: ' . mysqli_error($conn));
					}
				}
			}

			function mysqlCommand(){
				if (!isset($_POST['command']) || empty($_POST['command'])) {
					echo "<div class='status'­>Veuillez inscrire une command à exécuter.</div>";
				}
				else {
					$command = $_POST["command"];
					$sql = 	"$command";
					
					$retval = mysqli_query($conn, $sql);
					if(! $retval )
					{
						die('Could not do that: ' . mysqli_error($conn));
					}

					echo "<div class='status'­>Command successful!</div>";
				}
			}
			?>
	</body>
</html>