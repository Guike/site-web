﻿<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Vélomane - MySQL</title>
		<meta name="description" content="Site web du magasin Velomane" />
		<meta name="keywords" content="website, velomane, velo, carbone, gilles caron, guillaume docquier, sylvain bilodeau, hochelaga, montreal" />
		<meta name="author" content="Velomane" />
		<link rel="shortcut icon" href="../img/tab-icon.png">
		
		<link rel="stylesheet" type="text/css" href="styles.css" />

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="http://form-serialize.googlecode.com/svn/trunk/serialize-0.2.min.js" type="text/javascript"></script>
		
		<script>
			var modification = false;
			var nom;
			var	tel;
			var	desc;
			var status;

			function openModification(resultId) {
				//Disable clicking on other results
				modification = true;

				//Save default values
				nom = document.forms[resultId].elements[1].value;
				tel = document.forms[resultId].elements[2].value;
				desc = document.forms[resultId].elements[3].value;
				status = document.forms[resultId].elements[4].value;
				
				//Prevent hover effects on other results 
				jQuery('.result').addClass("unclickable");

				//Show/hide buttons
				jQuery('#box' + resultId + ' .primary').hide();
				jQuery('#box' + resultId + ' .modification').show();

				//Allow user to change inputs
				jQuery('#result' + resultId + ' input').prop('disabled', false);
				jQuery('#result' + resultId + ' input.undisablable').prop('disabled', true);
			}

			function closeModification(resultId, modified) {
				//Enable clicking on other results
				modification = false;

				//Allow hover effects on other results
				jQuery('.result').removeClass("unclickable");

				//Show/hide buttons
				jQuery('#box' + resultId + ' .primary').show();
				jQuery('#box' + resultId + ' .modification').hide();

				//Prevent user from changing inputs
				jQuery('#result' + resultId + ' input').prop('disabled', true);

				if (!modified) {
					//Restore default values
					document.forms[resultId].elements[1].value = nom; 
					document.forms[resultId].elements[2].value = tel;
					document.forms[resultId].elements[3].value = desc;
					document.forms[resultId].elements[4].value = status;
				}
				else databaseQuery('modify', resultId);
			}

			jQuery(document).on('click', '.result', function () {
				//If not editing a result
				if (!modification) {
					var boxNo = jQuery(this).attr('href');

					//Deactivate all results
					jQuery('.result').removeClass("active");

					if ($(boxNo).css('display') == 'none') {
						//Hide all buttons and show current buttons
						jQuery('.buttonWrap').hide();
						jQuery(boxNo).show();

						//Activate current result
						jQuery(this).addClass("active");
					}
					else {
						//Hide current buttons
						jQuery(boxNo).hide();
					}
				}
			});		
		</script>

		<script>
		function databaseQuery(request, formNo)
		{
			modification = false;
			var xmlhttp;
			if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else {// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200 && (request != 'modify'))
				{
					document.getElementById("commandResult").innerHTML=xmlhttp.responseText;
				}
			}

			//if (validate(formNo)) {
				var serializeData = serialize(document.forms[formNo]); 
			//	document.getElementById("serialized").innerHTML = serializeData;
				 
				xmlhttp.open("POST","mysql.php?request=" + request, true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send(serializeData);

			//	document.getElementById('addForm').reset();
			//}
		}
		</script>
		
		<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	</head>
	
	<body>		
		<div class="content">
			<div class='commands'>
				<h2>Recherche dans la base de donnée</h2>
				<form method="POST" id='searchForm' name="searchForm" enctype="multipart/form-data">
					<div class="inputWrap">
						<input type="text" name="search" id="search" placeholder="Recherche.."></input>
					</div>
					<input type="button" value=" Trouver un vélo " onclick="databaseQuery('search', 0)" class="bouton search">
				</form>
				<div class="clear"></div>

				<h2>Modification de la base de donnée</h2>
				<form method="POST" id='addForm' name="addForm" enctype="multipart/form-data">
					<div class="inputWrap">
						<input type="text" name="id" placeholder="#.."></input>
						<input type="text" name="nom" placeholder="Nom.."></input>
						<input type="text" name="tel" placeholder="Tel.."></input>
						<input type="text" name="desc" placeholder="Description.."></input>
					</div>
					<input type="button" value=" Ajouter un vélo " onclick="databaseQuery('add', 1)" class='bouton add'>
				</form>
				<div class="clear"></div>
				
				<form method="POST" id='deleteForm' name="deleteForm" enctype="multipart/form-data">
					<div class="inputWrap">
						<input type="text" name="id" placeholder="#.."></input>
					</div>
					<input type="button" value=" Supprimer un vélo " onclick="databaseQuery('delete', 2)" class='bouton delete'>
				</form>
				<div class="clear"></div>

				<!--<form method="POST" id='commandForm' name="commandForm" enctype="multipart/form-data">
					<div class="inputWrap">
						<input type="text" name="command" placeholder="Command line.."></input>
					</div>
					<input type="button" value=" Effectuer " onclick="databaseQuery('command', 3)" class='bouton command'>
				</form>
				<div class="clear spacer"></div>-->
			</div>

			<div class="topshadow">
					<img src="img/universal_top_shadow.png" />
			</div>

			<div id="commandResult"><!--Results-->				
				<!--<div class='status'­>
					2 résultats:
				</div>
				<div class='clear'></div>

				<div class='resultWrap'>
					<form  method="POST" name='resultForm' class='display'>
						<div class='result' id='result3' href='#box3'>
							Tag: <input type="text" name="id" value="2346" disabled class='undisablable'></input><br>
							Nom: <input type="text" name="nom" value="Antoine Dubuc" disabled></input><br>
							Téléphone: <input type="text" name="tel" value="514-234-1234" disabled></input><br>
							Date reçu: <span>2015-07-29</span><br>
							Description: <input type="text" name="desc" value="Giant TCR gris" disabled></input><br>
							Statut: <input type="text" name="status" value="En attente" disabled></input>
						</div>

						<div class='buttonWrap' id='box3'>
							<input type="button" value=" Supprimer " onclick="databaseQuery('delete', 3)" class='bouton delete primary'>
							<input type="button" value=" Modifier " onclick="modifyResult(3)" class='bouton modify primary'>
							<input type="button" value=" Confirmer " onclick="databaseQuery('modify', 3)" class='bouton add modification'>
							<input type="button" value=" Annuler " onclick="cancelModification(3)" class='bouton delete modification'>
						</div>
					</form>
					<div class='clear'></div>
				</div>

				<div class='resultWrap'>
					<form method="POST" name='resultForm' class='display'>
						<div class='result' id='result4' href='#box4'>
							Tag: <input type="text" name="id" value="6543" disabled></input><br>
							Nom: <input type="text" name="nom" value="Jean Marc" disabled></input><br>
							Téléphone: <input type="text" name="tel" value="514-214-4219" disabled></input><br>
							Date reçu: <span>2015-07-29</span><br>
							Description: <input type="text" name="desc" value="Felt Z100" disabled></input><br>
							Statut: <input type="text" name="status" value="En attente" disabled></input>
						</div>

						<div class='buttonWrap' id='box4'>
							<input type="button" value=" Supprimer " onclick="databaseQuery('delete', 4)" class='bouton delete primary'>
							<input type="button" value=" Modifier " onclick="modifyResult('4')" class='bouton modify primary'>
							<input type="button" value=" Confirmer " onclick="databaseQuery('modify', 4)" class='bouton add modification'>
							<input type="button" value=" Annuler " onclick="cancelModification('4')" class='bouton delete modification'>
						</div>
					</form>
					<div class='clear'></div>
				</div>
			</div>
			<div id="serialized"></div>-->
			<div class="spacer"></div>
		</div>
	</body>
</html>