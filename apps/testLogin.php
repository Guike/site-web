﻿<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>Vélomane - Login</title>
		<meta name="description" content="Site web du magasin Velomane" />
		<meta name="keywords" content="website, velomane, velo, carbone, gilles caron, guillaume docquier, sylvain bilodeau, hochelaga, montreal" />
		<meta name="author" content="Velomane" />
		<link rel="shortcut icon" href="../img/tab-icon.png">
		
		<link rel="stylesheet" type="text/css" href="styles.css" />

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="http://form-serialize.googlecode.com/svn/trunk/serialize-0.2.min.js" type="text/javascript"></script>
		
		
		
		<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	</head>
	<body id="body-color"> 
		<div id="Sign-In"> 
			<fieldset style="width:30%"><legend>LOG-IN HERE</legend> 
				<form method="POST" action="connectivity.php"> 
					User <br><input type="text" name="user" size="40"><br> 
					Password <br><input type="password" name="pass" size="40"><br> 
					<input id="button" type="submit" name="submit" value="Log-In"> 
				</form> 
			</fieldset> 
		</div> 
	</body> 
</html>
