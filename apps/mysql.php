﻿<!DOCTYPE html>
<html lang="en">
	<body>
			<?PHP
			include 'mysqlFunctions.php';

			$host = 'localhost';
			$user = 'root';
			$pass = 'temp4DB12+v';

			$conn = mysqli_connect($host, $user, $pass);
			if(! $conn )
			{
				die('Could not connect to database: ' . mysqli_error($conn));
			}
			
			$bdd = 'velomane';
			$select = mysqli_select_db($conn, "$bdd");
			if(! $select){
				die('Could not find database: ' . mysqli_error($conn));
			}

			/*Database format
			tag_lst (		tag_id int not null,
							tag_nom varchar(35) not null,
							tag_tel varchar(22) not null,
							tag_date DATE not null,
							tag_desc varchar(35) not null,								
							tag_status varchar(30) default 'En attente',
							primary key (tag_numero),
							index client_nom (tag_nom)
			)*/

			$request = $_REQUEST['request'];

			switch($request){
				case 'search':
					mysqlSearch($conn);
					break;
				case 'add':
					mysqlAdd($conn);
					break;
				case 'delete':
					mysqlDelete($conn);
					break;
				case 'createtable':
					mysqlCreateTable($conn);
					break;
				case 'modify':
					mysqlModify($conn);
					break;
				/*case 'command':
					mysqlCommand($conn);
					break;*/
			}

			mysqli_close($conn);
			?>
	</body>
</html>