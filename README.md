# Guide de codage #
Commenter le début et la fin des balises principales:
>
    <!-- Début container -->
    <div class="container">
        blahblah
    </div>
    <!-- Fin container -->

Effectuer une identation adéquate:
>
    <div>
        <div>
            <h2></h2>
            <div>
            </div>
        </div>
    </div>

***
# Plan du site #
1. Page d'accueil
    1. Promotions
    2. Actualité 
2. Page des produits
    1. Les vélos
    2. Les vêtements
3. Page des services
    1. Les mises au point
    2. Les positionnements
4. Page compagnie
    1. L'historique
    2. Les employés
5. Page contact
    1. Google map
    2. Heures d'ouverture, téléphone, twitter, etc